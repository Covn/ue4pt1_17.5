﻿#include <iostream>
class Vector
{
private:
    float x;
    float y;
    float z;
public:
    //вывод конкретной координаты вектора
    float getvctX()
    {
        return x;
    }
    float getvctY()
    {
        return y;
    }
    float getvctZ()
    {
        return z;
    }
    //задание конкретной координаты вектора
    void setvctX(float newX)
    {
        x = newX;
    }
    void setvctY(float newY)
    {
        y = newY;
    }
    void setvctZ(float newZ)
    {
        z = newZ;
    }
    //вычисление длинны вектора
    float getvctlength()
    {
        return (sqrt(x * x + y * y + z * z));
    }
};

int main()
{
    //создаём вектор
    Vector vct1;
    //задаём координаты вектора
    vct1.setvctX(3);
    vct1.setvctY(4);
    vct1.setvctZ(12);
    //выводим знач-е функции внутри класс
    std::cout << vct1.getvctlength();
}